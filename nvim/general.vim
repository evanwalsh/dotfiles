" Editor behavior
set tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab " 2 space tabs
set number " Show line number
set relativenumber " Show relative line number for lines other than the current
set clipboard=unnamedplus

"" Map FZF to \]
"nmap \] :FZF<cr>

"" Ruby
"let g:ruby_path = system('echo $HOME/.asdf/shims/ruby')
"let g:rufo_auto_formatting = 1 " Autoformat on save

" Map ESC to exit terminal insert mode
tnoremap <Esc> <C-\><C-n> 
autocmd BufWinEnter,WinEnter term://* startinsert " Start insert when entering terminal
autocmd BufLeave term://* stopinsert " Stop insert when leaving terminal
