if &compatible
  set nocompatible
endif

set runtimepath+=/home/evan/.cache/dein/repos/github.com/Shougo/dein.vim

if dein#load_state($HOME.'/.cache/dein')
  call dein#begin($HOME.'/.cache/dein')

  " Let dein manage dein
  call dein#add($HOME.'/.cache/dein/repos/github.com/Shougo/dein.vim')

  source $HOME/.config/nvim/plugins.vim

  call dein#end()
  call dein#save_state()
endif

" Required
filetype plugin indent on
syntax enable

" Install missing plugins on startup
if dein#check_install()
  call dein#install()
endif
