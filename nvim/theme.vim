" Enable true color
if (empty($TMUX))
  if(has("nvim"))
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
endif

" One Dark <3
colorscheme onedark

let g:lightline = {
  \ 'colorscheme': 'onedark',
  \ }
