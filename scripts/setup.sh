set +x

alias dlink='ln -sfn'

echo "Ensuring config directory"
mkdir -p $HOME/.config

echo "Linking gitconfig"
dlink $HOME/.dotfiles/gitconfig $HOME/.gitconfig

echo "Linking NeoVim config"
dlink $HOME/.dotfiles/nvim $HOME/.config/nvim

echo "Installing NeoVim package manager"
mkdir -p $HOME/.cache/dein
git clone https://github.com/Shougo/dein.vim $HOME/.cache/dein/repos/github.com/Shougo/dein.vim

unalias dlink

set -x
