@echo off
setlocal enableextensions

where choco >nul 2>nul

if %ERRORLEVEL%==1 (
  echo Installing Chocolatey
  @"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
) else (
  echo Chocolatey is present
)

rem Install packages with Chocolatey
choco install git neovim -y 

rem Create directories
mkdir %USERPROFILE%\.config

rem Link directories
mklink /D %APPDATA%\.config\nvim %USERPROFILE%\dotfiles\nvim

rem Link files
mklink %USERPROFILE%\.gitconfig %USERPROFILE%\dotfiles\gitconfig

endlocal
