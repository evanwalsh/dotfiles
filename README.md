# Evan Walsh's dotfiles

These are mine. I hope they help you somehow.

## Installation

```
git clone https://gitlab.com/evanwalsh/dotfiles.git ~/.dotfiles
```

## Usage

If you're on Linux, run `sh ./scripts/setup.sh`

If you're on Windows, run `.\scripts\setup.bat`
